from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from django.urls import resolve
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import unittest
import time


class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        self.assertEqual("Tweet Me!", self.browser.title)
        self.tearDown()

    def test_add_tweet(self):
        self.browser.get(self.live_server_url)
        time.sleep(3)
        tweet = self.browser.find_element_by_name('tweet')
        submit = self.browser.find_element_by_id('submit')
        tweet.send_keys('Coba Coba')
        time.sleep(5)
        submit.send_keys(Keys.RETURN)
        self.browser.get(self.live_server_url)
        self.assertIn('Coba Coba', self.browser.page_source)
        self.tearDown()


class TweetTest(TestCase):
    def test_tweetpage_is_exist(self):
        self.response = Client().get('')
        self.assertEqual(self.response.status_code, 200)

    def test_form_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</form>', self.response.content.decode())

    def test_halo_apa_kabar_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</h2>', self.response.content.decode())

    def test_tweetpage_is_not_exist(self):
        self.response = Client().get('error/')
        self.assertEqual(self.response.status_code, 404)

    def test_navbar_is_exist(self):
        self.response = Client().get('')
        self.assertIn('</nav>', self.response.content.decode())

    def test_profil_page_is_exist(self):
        self.response = Client().get('/profil/')
        self.assertEqual(self.response.status_code, 200)

    def test_photo_is_exist(self):
        self.response = Client().get('/profil/')
        self.assertIn('<img src="../static/Putsal.jpeg" class="rounded" style="width: 15rem; height: 10rem;" alt="Foto Profil">',
                      self.response.content.decode())

    def test_accordion_is_exist(self):
        self.response = Client().get('/profil/')
        self.assertIn('<button class="accordion">Aktivitas</button>',
                      self.response.content.decode())

    def test_accordion_panel_is_exist(self):
        self.response = Client().get('/profil/')
        self.assertIn('<div class="panel">', self.response.content.decode())

    def test_profil_page_is_not_exist(self):
        self.response = Client().get('error/')
        self.assertEqual(self.response.status_code, 404)

    def test_search_page_is_exist(self):
        self.response = Client().get('/search/')
        self.assertEqual(self.response.status_code, 200)

    def test_search_page_is_not_exist(self):
        self.response = Client().get('error/')
        self.assertEqual(self.response.status_code, 404)

    def test_login_page_is_exist(self):
        self.response = Client().get('/login/')
        self.assertEqual(self.response.status_code, 200)

    def test_url_name(self):
        self.found = resolve('/login/')
        self.assertEqual(self.found.url_name, 'login')

    def test_login_form_is_exist(self):
        self.response = Client().get('/login/')
        self.assertIn('</form>', self.response.content.decode())

    def test_login_page_is_not_exist(self):
        self.response = Client().get('error/')
        self.assertEqual(self.response.status_code, 404)

    def test_logout(self):
        self.client.logout()
        self.response = Client().get('')
        self.assertEqual(self.response.status_code, 200)

    def test_tutorial_page_is_exist(self):
        self.response = Client().get('/tutorial/')
        self.assertEqual(self.response.status_code, 200)

    def test_judul_tutorial_is_exist(self):
        self.response = Client().get('/tutorial/')
        self.assertIn('</h2>', self.response.content.decode())

    def test_tutorial_page_is_not_exist(self):
        self.response = Client().get('error/')
        self.assertEqual(self.response.status_code, 404)
