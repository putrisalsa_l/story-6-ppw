$("body").addClass(localStorage.getItem("theme"));

$('#autumn-red').hover(
  function() {
    $('body').addClass('red')
  },
  function() {
    $('body').removeClass('red')
  }
);

$('#winter-blue').hover(
    function() {
      $('body').addClass('blue')
    },
    function() {
      $('body').removeClass('blue')
    }
  );

$(document).ready(function() {
    $("#autumn-red").click(function() {
        localStorage.setItem("theme", "red-toggled");
        $("body").removeClass("blue-toggled");
        $("body").toggleClass("red-toggled");
    });
});

$(document).ready(function() {
  $("#winter-blue").click(function() {
      localStorage.setItem("theme", "blue-toggled");
      $("body").removeClass("red-toggled");
      $("body").toggleClass("blue-toggled");
  });
});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
