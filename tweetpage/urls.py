from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'tweetpage'

urlpatterns = [
    path('', views.tweet, name='tweet'),
    path('profil/', views.profil, name='profil'),
    path('search/', views.search, name='search'),
    path('tutorial/', views.tutorial, name='tutorial'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    url(r'tweet/(?P<pk>[0-9]+)/delete/$',
        views.tweetDelete.as_view(), name='tweetDelete')
]
