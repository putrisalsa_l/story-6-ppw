from django.shortcuts import render, redirect
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy
from .models import savedTweet
from .forms import tweetForm

from django.http import (
    HttpResponseNotAllowed, HttpResponseRedirect,
    HttpResponseForbidden, HttpResponseNotFound,
)
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, logout


class tweetDelete(DeleteView):
    model = savedTweet
    success_url = reverse_lazy('tweetpage:tweet')


def tweet(request):
    if request.method == 'POST':
        form = tweetForm(request.POST)

        if 'id' in request.POST:
            savedTweet.objects.get(id=request.POST['id']).delete()
            return redirect('/')

        if form.is_valid():
            # tweet = request.POST['tweet']
            tweet = savedTweet(
                tweet=form.data['tweet']
            )
            tweet.save()
            return redirect('/')

    else:
        form = tweetForm()

    tweet_content = {'events': savedTweet.objects.all().values(),
                     'form': form}
    return render(request, 'tweet.html', tweet_content)


def profil(request):
    return render(request, 'profil.html')


def search(request):
    return render(request, 'search.html')


def tutorial(request):
    return render(request, 'nginx.html')


def login_view(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')

    if request.method == 'GET':
        form = AuthenticationForm()
        login_content = {'form': form}
        return render(request, 'login.html', login_content)

    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return HttpResponseRedirect('/')

        else:
            # kalo error
            login_content = {'form': form}
            return render(request, 'login.html', login_content)


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')
